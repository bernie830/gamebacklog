//
//  QueueSpotDBHelper.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/4/22.
//

import Foundation
import SQLite3
import SwiftUI

class QueueSpotDBHelper {
    static var db : OpaquePointer?
    
    public static func createTable()  {
        let query = "CREATE TABLE IF NOT EXISTS queue_spot(id INTEGER PRIMARY KEY AUTOINCREMENT, queue_string TEXT, queue_sort_id INTEGER, background_red REAL, background_green REAL, background_blue REAL, text_red REAL, text_green REAL, text_blue REAL);"
        var statement : OpaquePointer? = nil
        
        if sqlite3_prepare_v2(self.db, query, -1, &statement, nil) == SQLITE_OK {
            if sqlite3_step(statement) == SQLITE_DONE {
                print("Table creation success")
            }else {
                print("Table creation fail")
            }
        } else {
            print("Prepration fail")
        }
    }
    
    public static func insert(queueSpot: QueueSpotModel) {
        
        var backgroundRed: CGFloat = 0
        var backgroundGreen: CGFloat = 0
        var backgroundBlue: CGFloat = 0
        var backgroundAlpha: CGFloat = 0
        
        UIColor(queueSpot.gameRowBackgroundColor).getRed(&backgroundRed, green: &backgroundGreen, blue: &backgroundBlue, alpha: &backgroundAlpha)
        
        var textRed: CGFloat = 0
        var textGreen: CGFloat = 0
        var textBlue: CGFloat = 0
        var textAlpha: CGFloat = 0
        UIColor(queueSpot.gameRowTextColor).getRed(&textRed, green: &textGreen, blue: &textBlue, alpha: &textAlpha)
        
        let insertStatementString = "INSERT INTO queue_spot(queue_string, queue_sort_id, background_red, background_green, background_blue, text_red, text_green, text_blue) VALUES (?, ?, ?, ?, ?, ?, ?, ?);"
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(insertStatement, 1, (queueSpot.queueStringID as NSString).utf8String, -1, nil)
            sqlite3_bind_int(insertStatement, 2, Int32(queueSpot.queueSortId))
            sqlite3_bind_double(insertStatement, 3, DatabaseHelper.cgFloatToDbl(float: backgroundRed))
            sqlite3_bind_double(insertStatement, 4, DatabaseHelper.cgFloatToDbl(float: backgroundGreen))
            sqlite3_bind_double(insertStatement, 5, DatabaseHelper.cgFloatToDbl(float: backgroundBlue))
            
            sqlite3_bind_double(insertStatement, 6, DatabaseHelper.cgFloatToDbl(float: textRed))
            sqlite3_bind_double(insertStatement, 7, DatabaseHelper.cgFloatToDbl(float: textGreen))
            sqlite3_bind_double(insertStatement, 8, DatabaseHelper.cgFloatToDbl(float: textBlue))
            
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        sqlite3_finalize(insertStatement)
    }
    
    public static func update(queueSpot: QueueSpotModel) {
        var backgroundRed: CGFloat = 0
        var backgroundGreen: CGFloat = 0
        var backgroundBlue: CGFloat = 0
        var backgroundAlpha: CGFloat = 0
        UIColor(queueSpot.gameRowBackgroundColor).getRed(&backgroundRed, green: &backgroundGreen, blue: &backgroundBlue, alpha: &backgroundAlpha)
        
        var textRed: CGFloat = 0
        var textGreen: CGFloat = 0
        var textBlue: CGFloat = 0
        var textAlpha: CGFloat = 0
        UIColor(queueSpot.gameRowTextColor).getRed(&textRed, green: &textGreen, blue: &textBlue, alpha: &textAlpha)
        
        let updateStatementString = "UPDATE queue_spot SET queue_string=?, queue_sort_id=?, background_red=?, background_green=?, background_blue=?, text_red=?, text_green=?, text_blue=? WHERE id=?;"
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(updateStatement, 1, (queueSpot.queueStringID as NSString).utf8String, -1, nil)
            sqlite3_bind_int(updateStatement, 2, Int32(queueSpot.queueSortId))
            sqlite3_bind_double(updateStatement, 3, Double(backgroundRed))
            sqlite3_bind_double(updateStatement, 4, Double(backgroundGreen))
            sqlite3_bind_double(updateStatement, 5, Double(backgroundBlue))
            
            sqlite3_bind_double(updateStatement, 6, Double(textRed))
            sqlite3_bind_double(updateStatement, 7, Double(textGreen))
            sqlite3_bind_double(updateStatement, 8, Double(textBlue))
            sqlite3_bind_int(updateStatement, 9, Int32(queueSpot.id))
            
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully updated row.")
            } else {
                print("Could not update row.")
            }
        } else {
            print("UPDATE statement could not be prepared.")
        }
        sqlite3_finalize(updateStatement)
    }
    
    public static func getID(queueSpot: QueueSpotModel) -> Int {
        let selectStatementStr = "SELECT id FROM queue_spot WHERE queue_string=? AND queue_sort_id=? AND background_red=? AND background_green=? AND background_blue=? AND text_red=? AND text_green=? AND text_blue=?;"
        
        var backgroundRed: CGFloat = 0
        var backgroundGreen: CGFloat = 0
        var backgroundBlue: CGFloat = 0
        var backgroundAlpha: CGFloat = 0
        UIColor(queueSpot.gameRowBackgroundColor).getRed(&backgroundRed, green: &backgroundGreen, blue: &backgroundBlue, alpha: &backgroundAlpha)
        
        var textRed: CGFloat = 0
        var textGreen: CGFloat = 0
        var textBlue: CGFloat = 0
        var textAlpha: CGFloat = 0
        UIColor(queueSpot.gameRowTextColor).getRed(&textRed, green: &textGreen, blue: &textBlue, alpha: &textAlpha)
        
        var queryStatement: OpaquePointer? = nil
        var id: Int32 = -1
        if sqlite3_prepare_v2(db, selectStatementStr, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(queryStatement, 1, (queueSpot.queueStringID as NSString).utf8String, -1, nil)
            sqlite3_bind_int(queryStatement, 2, Int32(queueSpot.queueSortId))
            
            sqlite3_bind_double(queryStatement, 3, DatabaseHelper.cgFloatToDbl(float: backgroundRed))
            sqlite3_bind_double(queryStatement, 4, DatabaseHelper.cgFloatToDbl(float: backgroundGreen))
            sqlite3_bind_double(queryStatement, 5, DatabaseHelper.cgFloatToDbl(float: backgroundBlue))
            
            sqlite3_bind_double(queryStatement, 6, DatabaseHelper.cgFloatToDbl(float: textRed))
            sqlite3_bind_double(queryStatement, 7, DatabaseHelper.cgFloatToDbl(float: textGreen))
            sqlite3_bind_double(queryStatement, 8, DatabaseHelper.cgFloatToDbl(float: textBlue))
            
            while sqlite3_step(queryStatement) == SQLITE_ROW  {
                if(id < 0) {
                    id = sqlite3_column_int(queryStatement, 0)
                }
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        return Int(id)
    }
    
    public static func getAllQueueSpots() -> [QueueSpotModel] {
        let selectStatementStr = "SELECT id, queue_string, queue_sort_id, background_red, background_green, background_blue, text_red, text_green, text_blue FROM queue_spot;"
        
        var allQueueSpots: [QueueSpotModel] = []
        
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, selectStatementStr, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW  {
                let id = sqlite3_column_int(queryStatement, 0)
                let queueString = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let queueSortId = sqlite3_column_int(queryStatement, 2)
                let backgroundRed = sqlite3_column_double(queryStatement, 3)
                let backgroundGreen = sqlite3_column_double(queryStatement, 4)
                let backgroundBlue = sqlite3_column_double(queryStatement, 5)
                let textRed = sqlite3_column_double(queryStatement, 6)
                let textGreen = sqlite3_column_double(queryStatement, 7)
                let textBlue = sqlite3_column_double(queryStatement, 8)
                
                let gameRowBackgroundColor: Color = Color(decimalRed: backgroundRed, green: backgroundGreen, blue: backgroundBlue)
                let gameRowTextColor: Color = Color(decimalRed: textRed, green: textGreen, blue: textBlue)
                
                
                allQueueSpots.append(QueueSpotModel(id: Int(id), queueString: queueString, queueSortId: Int(queueSortId), gameRowBackgroundColor: gameRowBackgroundColor, gameRowTextColor: gameRowTextColor))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        
        return allQueueSpots
    }
    
    public static func getQueueSpot(id: Int) -> QueueSpotModel {
        let selectStatementStr = "SELECT queue_string, queue_sort_id, background_red, background_green, background_blue, text_red, text_green, text_blue FROM queue_spot WHERE id=?;"
        
        var queueString: String = ""
        var queueSortId: Int32 = -1
        var backgroundRed: Double = -1
        var backgroundGreen: Double = -1
        var backgroundBlue: Double = -1
        var textRed: Double = -1
        var textGreen: Double = -1
        var textBlue: Double = -1
        
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, selectStatementStr, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(id))
            
            while sqlite3_step(queryStatement) == SQLITE_ROW  {
                queueString = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                queueSortId = sqlite3_column_int(queryStatement, 1)
                backgroundRed = sqlite3_column_double(queryStatement, 2)
                backgroundGreen = sqlite3_column_double(queryStatement, 3)
                backgroundBlue = sqlite3_column_double(queryStatement, 4)
                textRed = sqlite3_column_double(queryStatement, 5)
                textGreen = sqlite3_column_double(queryStatement, 6)
                textBlue = sqlite3_column_double(queryStatement, 7)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        
        let gameRowBackgroundColor: Color = Color(decimalRed: backgroundRed, green: backgroundGreen, blue: backgroundBlue)
        let gameRowTextColor: Color = Color(decimalRed: textRed, green: textGreen, blue: textBlue)
        
        
        return QueueSpotModel(id: Int(id), queueString: queueString, queueSortId: Int(queueSortId), gameRowBackgroundColor: gameRowBackgroundColor, gameRowTextColor: gameRowTextColor)
    }
    
    public static func dropTable()  {
        let dropStatementString = "DROP TABLE queue_spot;"
        var dropStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, dropStatementString, -1, &dropStatement, nil) == SQLITE_OK {
            if sqlite3_step(dropStatement) == SQLITE_DONE {
                print("Successfully dropped table.")
            } else {
                print("Could not drop table.")
            }
        } else {
            print("DROP statement could not be prepared.")
        }
        sqlite3_finalize(dropStatement)
    }
}
