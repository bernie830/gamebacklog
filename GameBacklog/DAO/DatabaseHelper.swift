//
//  DatabaseHelper.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/3/22.
//

import Foundation
import SwiftUI
import SQLite3

class DatabaseHelper {
    var db : OpaquePointer?
    var path : String = "GameBacklogDatabase.sqlite"
    
    
    init() {
        db = openDatabase()
        createOrOpenTables(resetData: true)
    }
    
    private func openDatabase() -> OpaquePointer? {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    .appendingPathComponent(path)
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            print("Successfully opened connection to database at \(path)")
            return db
        } else {
            print("error opening database")
            return nil
        }
    }
    
    private func createOrOpenTables(resetData: Bool) {
        setHelperDBPointers()
        
        if resetData {
            dropAllTables()
        }
        
        GeneralGameDBHelper.createTable()
        QueueSpotDBHelper.createTable()
        GamePlatformDBHelper.createTable()
        PersonalGameDBHelper.createTable()
        
        if resetData {
            var titleToGeneralGameIds:[String:Int] = [:]
            var queueStringToQueueId:[String:Int] = [:]

            let fakeDataGames = TestEnvironment.getFakeData()
            var index = 0
            while index < fakeDataGames.count {
                var game = fakeDataGames[index]
                let title = game.generalGame.title
                var generalGameId = -1
                
                if titleToGeneralGameIds[title] == nil {
                    GeneralGameDBHelper.insert(game: game.generalGame)
                    generalGameId = GeneralGameDBHelper.getID(game: game.generalGame)
                    titleToGeneralGameIds[title] = generalGameId
                } else {
                    generalGameId = titleToGeneralGameIds[title]!
                }
                
                var queueSpotId = -1
                let queueString = game.queueStatus.queueStringID
                if queueStringToQueueId[queueString] == nil {
                    QueueSpotDBHelper.insert(queueSpot: game.queueStatus)
                    queueSpotId = QueueSpotDBHelper.getID(queueSpot: game.queueStatus)
                    queueStringToQueueId[queueString] = queueSpotId
                } else {
                    queueSpotId = queueStringToQueueId[queueString]!
                }

                GamePlatformDBHelper.insert(platform: game.gameOnPlatform)
                let platformId = GamePlatformDBHelper.getID(platform: game.gameOnPlatform)
                
                game.gameOnPlatform.id = platformId
                
                let queueSpotModel = QueueSpotModel(id: queueSpotId, queueString: game.queueStatus.queueStringID, queueSortId: game.queueStatus.queueSortId, gameRowBackgroundColor: game.queueStatus.gameRowBackgroundColor, gameRowTextColor: game.queueStatus.gameRowTextColor)
                let gamePlatformModel = GamePlatformModel(id: platformId, platformStr: game.gameOnPlatform.platform, storefrontStr: game.gameOnPlatform.storefront, releaseWindowStr: game.gameOnPlatform.releaseWindow, releaseDate: game.gameOnPlatform.platformReleaseDate, mostRecentPriceDbl: game.gameOnPlatform.mostRecentPrice, generalGameId: generalGameId)
                
                let generalGame = GeneralGameModel(id: generalGameId, titleStr: game.generalGame.title, gameplayType: game.generalGame.mainGameplayType, storyFocused: game.generalGame.isStoryFocused)
                
                let personalGame = PersonalGameModel(id: -1, generalGame: generalGame, platform: gamePlatformModel, queueSpot: queueSpotModel, priceIdPayDbl: game.priceIdPay, isPurchased: game.hasBeenPurchased)
                PersonalGameDBHelper.insert(game: personalGame)
                
                index = index + 1
            }
        }
    }
    
    private func setHelperDBPointers() {
        GeneralGameDBHelper.db = self.db
        PersonalGameDBHelper.db = self.db
        QueueSpotDBHelper.db = self.db
        GamePlatformDBHelper.db = self.db
    }
    
    private func dropTable(table: String) {
        let dropStatementString = "DROP TABLE " + table + ";"
        var dropStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, dropStatementString, -1, &dropStatement, nil) == SQLITE_OK {
            if sqlite3_step(dropStatement) == SQLITE_DONE {
                print("Successfully dropped table.")
            } else {
                print("Could not drop table.")
            }
        } else {
            print("DROP statement could not be prepared.")
        }
        sqlite3_finalize(dropStatement)
    }
    
    private func dropAllTables() {
        PersonalGameDBHelper.dropTable()
        GamePlatformDBHelper.dropTable()
        QueueSpotDBHelper.dropTable()
        GeneralGameDBHelper.dropTable()
    }
    
    /*
     Callable functions
     */
    func getAllGames() -> [PersonalGameModel]{
        return PersonalGameDBHelper.getAllPersonalGames()
    }
    
    func getAllQueueSpots() -> [QueueSpotModel] {
        return QueueSpotDBHelper.getAllQueueSpots()
    }
    
    func updateQueue(game: PersonalGameModel) {
        
    }
    
    
    /*
     Util Functions
     */
    public static func boolToInt32(inBool: Bool) -> Int32{
        var retVal: Int32 = 0
        if(inBool) {
            retVal = 1
        }
        return retVal
    }
    
    public static func intToBool(val: Int) -> Bool{
        return val == 1
    }
    
    public static func dateToDBStringFormat(date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: date)
    }
    
    public static func dbStringFormatToDate(date: String) -> Date{
        let dateFormatter = DateFormatter()
        if date.count == 10 {
            dateFormatter.dateFormat = "yyyy-MM-dd"
        } else {
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        }
        
        return dateFormatter.date(from: date)!
    }
    
    public static func cgFloatToDbl(float: CGFloat) -> Double {
        return Double(float)*255
    }
    
    
}
