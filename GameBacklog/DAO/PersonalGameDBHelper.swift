//
//  PersonalGameDBHelper.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/4/22.
//

import Foundation
import SQLite3

class PersonalGameDBHelper {
    static var db : OpaquePointer?
    
    public static func createTable()  {
        let query = "CREATE TABLE IF NOT EXISTS personal_game(id INTEGER PRIMARY KEY AUTOINCREMENT, buy_now_price REAL, has_been_purchased INTEGER, general_game_id INTEGER, queue_spot_id INTEGER, game_platform_id INTEGER);"
        var statement : OpaquePointer? = nil
        
        if sqlite3_prepare_v2(self.db, query, -1, &statement, nil) == SQLITE_OK {
            if sqlite3_step(statement) == SQLITE_DONE {
                print("Table creation success")
            }else {
                print("Table creation fail")
            }
        } else {
            print("Prepration fail")
        }
    }
    
    public static func insertAlreadyCreatedReferences(game: PersonalGameModel) {
        let insertStatementString = "INSERT INTO personal_game(buy_now_price, has_been_purchased, general_game_id, queue_spot_id, game_platform_id) VALUES (?, ?, ?, ?, ?);"
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_double(insertStatement, 1, Double(game.priceIdPay))
            sqlite3_bind_int(insertStatement, 2, DatabaseHelper.boolToInt32(inBool: game.hasBeenPurchased))
            sqlite3_bind_int(insertStatement, 3, game.generalGameId)
            sqlite3_bind_int(insertStatement, 4, game.queueSpotId)
            sqlite3_bind_int(insertStatement, 5, game.gamePlatformId)
            
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        sqlite3_finalize(insertStatement)
    }
    
    public static func insert(game: PersonalGameModel) {
        let insertStatementString = "INSERT INTO personal_game(buy_now_price, has_been_purchased, general_game_id, queue_spot_id, game_platform_id) VALUES (?, ?, ?, ?, ?);"
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_double(insertStatement, 1, Double(game.priceIdPay))
            sqlite3_bind_int(insertStatement, 2, DatabaseHelper.boolToInt32(inBool: game.hasBeenPurchased))
            sqlite3_bind_int(insertStatement, 3, Int32(game.generalGameId))
            sqlite3_bind_int(insertStatement, 4, Int32(game.queueSpotId))
            sqlite3_bind_int(insertStatement, 5, Int32(game.gameOnPlatform.id))
            
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        sqlite3_finalize(insertStatement)
    }
    
    public static func update(game: PersonalGameModel) {
        let updateStatementString = "UPDATE personal_game SET buy_now_price=?, has_been_purchased=?, general_game_id=?, queue_spot_id=?, game_platform_id=? WHERE id=?;"
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            sqlite3_bind_double(updateStatement, 1, Double(game.priceIdPay))
            sqlite3_bind_int(updateStatement, 2, DatabaseHelper.boolToInt32(inBool: game.hasBeenPurchased))
            sqlite3_bind_int(updateStatement, 3, game.generalGameId)
            sqlite3_bind_int(updateStatement, 4, game.queueSpotId)
            sqlite3_bind_int(updateStatement, 5, game.gamePlatformId)
            sqlite3_bind_int(updateStatement, 6, Int32(game.id))
            
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully updated row.")
            } else {
                print("Could not update row.")
            }
        } else {
            print("UPDATE statement could not be prepared.")
        }
        sqlite3_finalize(updateStatement)
    }
    
    public static func updateQueueSpot(id: Int, newQueueSpotId: Int) {
        let updateStatementString = "UPDATE personal_game SET queue_spot_id=? WHERE id=?;"
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(updateStatement, 1, Int32(newQueueSpotId))
            sqlite3_bind_int(updateStatement, 2, Int32(id))
            
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully updated row.")
            } else {
                print("Could not update row.")
            }
        } else {
            print("UPDATE statement could not be prepared.")
        }
        sqlite3_finalize(updateStatement)
    }
    
    public static func delete(id: Int) {
        let deleteStatementString = "DELETE FROM personal_game WHERE id=?;"
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, deleteStatementString, -1, &deleteStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(deleteStatement, 1, Int32(id))
            
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                print("Successfully deleted row " + String(id))
            } else {
                print("Could not delete row.")
            }
        } else {
            print("DELETE statement could not be prepared.")
        }
        sqlite3_finalize(deleteStatement)
    }
    
    public static func getID(game: PersonalGameModel) -> Int {
        let selectStatementStr = "SELECT id FROM personal_game WHERE buy_now_price=? AND has_been_purchased=? AND general_game_id=? AND queue_spot_id=? AND game_platform_id=?;"
        
        var queryStatement: OpaquePointer? = nil
        var id: Int32 = -1
        if sqlite3_prepare_v2(db, selectStatementStr, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_double(queryStatement, 1, Double(game.priceIdPay))
            sqlite3_bind_int(queryStatement, 2, DatabaseHelper.boolToInt32(inBool: game.hasBeenPurchased))
            sqlite3_bind_int(queryStatement, 3, game.generalGameId)
            sqlite3_bind_int(queryStatement, 4, game.queueSpotId)
            sqlite3_bind_int(queryStatement, 5, game.gamePlatformId)
            
            while sqlite3_step(queryStatement) == SQLITE_ROW  {
                if(id < 0) {
                    id = sqlite3_column_int(queryStatement, 0)
                }
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        return Int(id)
    }
    
    public static func getPersonalGame(id: Int) -> PersonalGameModel {
        let selectStatementStr = "SELECT buy_now_price, has_been_purchased, general_game_id, queue_spot_id, game_platform_id FROM personal_game WHERE id=?;"
        
        var buyNowPrice: Double = -1
        var hasBeenPurchased: Int32 = -1
        var generalGameId: Int32 = -1
        var queueSpotId: Int32 = -1
        var gamePlatformId: Int32 = -1
        
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, selectStatementStr, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(id))
            
            while sqlite3_step(queryStatement) == SQLITE_ROW  {
                buyNowPrice = sqlite3_column_double(queryStatement, 0)
                hasBeenPurchased = sqlite3_column_int(queryStatement, 1)
                generalGameId = sqlite3_column_int(queryStatement, 2)
                queueSpotId = sqlite3_column_int(queryStatement, 3)
                gamePlatformId = sqlite3_column_int(queryStatement, 4)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        
        
        let generalGame = GeneralGameDBHelper.getGeneralGame(id: Int(generalGameId))
        let queueSpot = QueueSpotDBHelper.getQueueSpot(id: Int(queueSpotId))
        let gamePlatform = GamePlatformDBHelper.getGamePlatform(id: Int(gamePlatformId))
        
        return PersonalGameModel(id: id, generalGame: generalGame, platform: gamePlatform, queueSpot: queueSpot, priceIdPayDbl: buyNowPrice, isPurchased: DatabaseHelper.intToBool(val: Int(hasBeenPurchased)))
    }
    
    public static func getAllPersonalGames() -> [PersonalGameModel] {
        let selectStatementStr = "SELECT id, buy_now_price, has_been_purchased, general_game_id, queue_spot_id, game_platform_id FROM personal_game;"
        
        var personalGames: [PersonalGameModel] = []
        
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, selectStatementStr, -1, &queryStatement, nil) == SQLITE_OK {
            
            while sqlite3_step(queryStatement) == SQLITE_ROW  {
                let id = sqlite3_column_int(queryStatement, 0)
                let buyNowPrice = sqlite3_column_double(queryStatement, 1)
                let hasBeenPurchased = sqlite3_column_int(queryStatement, 2)
                let generalGameId = sqlite3_column_int(queryStatement, 3)
                let queueSpotId = sqlite3_column_int(queryStatement, 4)
                let gamePlatformId = sqlite3_column_int(queryStatement, 5)
                
                if generalGameId > 0 && queueSpotId > 0 && gamePlatformId > 0 {
                    let generalGame = GeneralGameDBHelper.getGeneralGame(id: Int(generalGameId))
                    let queueSpot = QueueSpotDBHelper.getQueueSpot(id: Int(queueSpotId))
                    let gamePlatform = GamePlatformDBHelper.getGamePlatform(id: Int(gamePlatformId))

                    personalGames.append(PersonalGameModel(id: Int(id), generalGame: generalGame, platform: gamePlatform, queueSpot: queueSpot, priceIdPayDbl: buyNowPrice, isPurchased: DatabaseHelper.intToBool(val: Int(hasBeenPurchased))))
                }
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        
        return personalGames
    }
    
    public static func dropTable()  {
        let dropStatementString = "DROP TABLE personal_game;"
        var dropStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, dropStatementString, -1, &dropStatement, nil) == SQLITE_OK {
            if sqlite3_step(dropStatement) == SQLITE_DONE {
                print("Successfully dropped table.")
            } else {
                print("Could not drop table.")
            }
        } else {
            print("DROP statement could not be prepared.")
        }
        sqlite3_finalize(dropStatement)
    }
    
}
