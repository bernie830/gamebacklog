//
//  GamePlatformDBHelper.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/4/22.
//

import Foundation
import SQLite3

class GamePlatformDBHelper {
    static var db : OpaquePointer?
    
    public static func createTable()  {
        let query = "CREATE TABLE IF NOT EXISTS game_platform(id INTEGER PRIMARY KEY AUTOINCREMENT, general_game_id INTEGER, platform TEXT, storefront TEXT, platform_release_date TEXT, release_window TEXT, most_recent_price REAL);"
        var statement : OpaquePointer? = nil
        
        if sqlite3_prepare_v2(self.db, query, -1, &statement, nil) == SQLITE_OK {
            if sqlite3_step(statement) == SQLITE_DONE {
                print("Table creation success")
            }else {
                print("Table creation fail")
            }
        } else {
            print("Prepration fail")
        }
    }
    
    public static func insert(platform: GamePlatformModel) {
        let insertStatementString = "INSERT INTO game_platform(general_game_id, platform, storefront, platform_release_date, release_window, most_recent_price) VALUES (?, ?, ?, ?, ?, ?);"
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(insertStatement, 1, Int32(platform.generalGameId))
            sqlite3_bind_text(insertStatement, 2, (platform.platform as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 3, (platform.storefront as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 4, (DatabaseHelper.dateToDBStringFormat(date: platform.platformReleaseDate) as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 5, (platform.releaseWindow as NSString).utf8String, -1, nil)
            sqlite3_bind_double(insertStatement, 6, Double(platform.mostRecentPrice))
            
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        sqlite3_finalize(insertStatement)
    }
    
    public static func update(platform: GamePlatformModel) {
        let updateStatementString = "UPDATE game_platform SET general_game_id=?, platform=?, storefront=?, platform_release_date=?, release_window=?, most_recent_price=? WHERE id=?;"
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(updateStatement, 1, Int32(platform.generalGameId))
            sqlite3_bind_text(updateStatement, 2, (platform.platform as NSString).utf8String, -1, nil)
            sqlite3_bind_text(updateStatement, 3, (platform.storefront as NSString).utf8String, -1, nil)
            sqlite3_bind_text(updateStatement, 4, (DatabaseHelper.dateToDBStringFormat(date: platform.platformReleaseDate) as NSString).utf8String, -1, nil)
            sqlite3_bind_text(updateStatement, 5, (platform.releaseWindow as NSString).utf8String, -1, nil)
            sqlite3_bind_double(updateStatement, 6, Double(platform.mostRecentPrice))
            sqlite3_bind_int(updateStatement, 7, Int32(platform.id))
            
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully updated row.")
            } else {
                print("Could not update row.")
            }
        } else {
            print("UPDATE statement could not be prepared.")
        }
        sqlite3_finalize(updateStatement)
    }
    
    public static func getID(platform: GamePlatformModel) -> Int {
        let selectStatementStr = "SELECT id FROM game_platform WHERE general_game_id=? AND platform=? AND storefront=? AND platform_release_date=? AND release_window=? AND most_recent_price=?;"
        
        var queryStatement: OpaquePointer? = nil
        var id: Int32 = -1
        if sqlite3_prepare_v2(db, selectStatementStr, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(platform.generalGameId))
            sqlite3_bind_text(queryStatement, 2, (platform.platform as NSString).utf8String, -1, nil)
            sqlite3_bind_text(queryStatement, 3, (platform.storefront as NSString).utf8String, -1, nil)
            sqlite3_bind_text(queryStatement, 4, (DatabaseHelper.dateToDBStringFormat(date: platform.platformReleaseDate) as NSString).utf8String, -1, nil)
            sqlite3_bind_text(queryStatement, 5, (platform.releaseWindow as NSString).utf8String, -1, nil)
            sqlite3_bind_double(queryStatement, 6, Double(platform.mostRecentPrice))
            
            while sqlite3_step(queryStatement) == SQLITE_ROW  {
                if(id < 0) {
                    id = sqlite3_column_int(queryStatement, 0)
                }
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        return Int(id)
    }
    
    public static func getGamePlatform(id: Int) -> GamePlatformModel {
        let selectStatementStr = "SELECT general_game_id, platform, storefront, platform_release_date, release_window, most_recent_price FROM game_platform WHERE id=?;"
        
        var generalGameId: Int32 = -1
        var platform: String = ""
        var storefront: String = ""
        var platformReleaseDateStr: String = ""
        var releaseWindowStr: String = ""
        var mostRecentPrice: Double = -1
        
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, selectStatementStr, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(id))
            
            while sqlite3_step(queryStatement) == SQLITE_ROW  {
                generalGameId = sqlite3_column_int(queryStatement, 0)
                platform = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                storefront = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                platformReleaseDateStr = String(describing: String(cString: sqlite3_column_text(queryStatement, 3)))
                releaseWindowStr = String(describing: String(cString: sqlite3_column_text(queryStatement, 4)))
                mostRecentPrice = sqlite3_column_double(queryStatement, 5)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        return GamePlatformModel(id: id, platformStr: platform, storefrontStr: storefront, releaseWindowStr: releaseWindowStr, releaseDate: DatabaseHelper.dbStringFormatToDate(date: platformReleaseDateStr), mostRecentPriceDbl: mostRecentPrice, generalGameId: Int(generalGameId))
    }
    
    public static func getGamePlatforms(generalGameId: Int) -> [GamePlatformModel] {
        let selectStatementStr = "SELECT id, platform, storefront, platform_release_date, release_window, most_recent_price FROM game_platform WHERE general_game_id=?;"
        
        var gamePlatforms: [GamePlatformModel] = []
        
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, selectStatementStr, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(generalGameId))
            
            while sqlite3_step(queryStatement) == SQLITE_ROW  {
                let id = sqlite3_column_int(queryStatement, 0)
                let platform = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let storefront = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                let platformReleaseDateStr = String(describing: String(cString: sqlite3_column_text(queryStatement, 3)))
                let releaseWindowStr = String(describing: String(cString: sqlite3_column_text(queryStatement, 4)))
                let mostRecentPrice = sqlite3_column_double(queryStatement, 5)
                
                gamePlatforms.append(GamePlatformModel(id: Int(id), platformStr: platform, storefrontStr: storefront, releaseWindowStr: releaseWindowStr, releaseDate: DatabaseHelper.dbStringFormatToDate(date: platformReleaseDateStr), mostRecentPriceDbl: mostRecentPrice, generalGameId: Int(generalGameId)))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        return gamePlatforms
    }
    
    public static func dropTable()  {
        let dropStatementString = "DROP TABLE game_platform;"
        var dropStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, dropStatementString, -1, &dropStatement, nil) == SQLITE_OK {
            if sqlite3_step(dropStatement) == SQLITE_DONE {
                print("Successfully dropped table.")
            } else {
                print("Could not drop table.")
            }
        } else {
            print("DROP statement could not be prepared.")
        }
        sqlite3_finalize(dropStatement)
    }
}
