//
//  GeneralGameDBHelper.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/4/22.
//

import Foundation
import SQLite3

class GeneralGameDBHelper {
    static var db : OpaquePointer?
    
    public static func createTable()  {
        let query = "CREATE TABLE IF NOT EXISTS general_game(id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, main_gameplay_type TEXT, story_focused_flag INTEGER);"
        var statement : OpaquePointer? = nil
        
        if sqlite3_prepare_v2(self.db, query, -1, &statement, nil) == SQLITE_OK {
            if sqlite3_step(statement) == SQLITE_DONE {
                print("Table creation success")
            }else {
                print("Table creation fail")
            }
        } else {
            print("Prepration fail")
        }
    }
    
    public static func insert(game: GeneralGameModel) {
        let insertStatementString = "INSERT INTO general_game(title, main_gameplay_type, story_focused_flag) VALUES (?, ?, ?);"
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(insertStatement, 1, (game.title as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 2, (game.mainGameplayType as NSString).utf8String, -1, nil)
            sqlite3_bind_int(insertStatement, 3, DatabaseHelper.boolToInt32(inBool: game.isStoryFocused))
            
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        sqlite3_finalize(insertStatement)
    }
    
    public static func update(game: GeneralGameModel) {
        let updateStatementString = "UPDATE general_game SET title=?, main_gameplay_type=?, story_focused_flag=? WHERE id=?;"
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(updateStatement, 1, (game.title as NSString).utf8String, -1, nil)
            sqlite3_bind_text(updateStatement, 2, (game.mainGameplayType as NSString).utf8String, -1, nil)
            sqlite3_bind_int(updateStatement, 3, DatabaseHelper.boolToInt32(inBool: game.isStoryFocused))
            sqlite3_bind_int(updateStatement, 4, Int32(game.id))
            
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully updated row.")
            } else {
                print("Could not update row.")
            }
        } else {
            print("UPDATE statement could not be prepared.")
        }
        sqlite3_finalize(updateStatement)
    }
    
    public static func getID(game: GeneralGameModel) -> Int {
        let selectStatementStr = "SELECT id FROM general_game WHERE title=? AND main_gameplay_type=? AND story_focused_flag=?;"
        
        var queryStatement: OpaquePointer? = nil
        var id: Int32 = -1
        if sqlite3_prepare_v2(db, selectStatementStr, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(queryStatement, 1, (game.title as NSString).utf8String, -1, nil)
            sqlite3_bind_text(queryStatement, 2, (game.mainGameplayType as NSString).utf8String, -1, nil)
            sqlite3_bind_int(queryStatement, 3, DatabaseHelper.boolToInt32(inBool: game.isStoryFocused))
            
            while sqlite3_step(queryStatement) == SQLITE_ROW  {
                if(id < 0) {
                    id = sqlite3_column_int(queryStatement, 0)
                }
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        return Int(id)
    }
    
    public static func getGeneralGame(id: Int) -> GeneralGameModel {
        let selectStatementStr = "SELECT title, main_gameplay_type, story_focused_flag FROM general_game WHERE id=?;"
        
        var title: String = ""
        var mainGameplayType: String = ""
        var storyFocusedFlag: Int32 = -1
        
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, selectStatementStr, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(id))
            
            while sqlite3_step(queryStatement) == SQLITE_ROW  {
                title = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                mainGameplayType = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                storyFocusedFlag = sqlite3_column_int(queryStatement, 2)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        return GeneralGameModel(id: id, titleStr: title, gameplayType: mainGameplayType, storyFocused: DatabaseHelper.intToBool(val: Int(storyFocusedFlag)))
    }
    
    public static func dropTable()  {
        let dropStatementString = "DROP TABLE general_game;"
        var dropStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, dropStatementString, -1, &dropStatement, nil) == SQLITE_OK {
            if sqlite3_step(dropStatement) == SQLITE_DONE {
                print("Successfully dropped table.")
            } else {
                print("Could not drop table.")
            }
        } else {
            print("DROP statement could not be prepared.")
        }
        sqlite3_finalize(dropStatement)
    }
    
    public static func getByTitle(title: String) -> GeneralGameModel {
        let selectStatementStr = "SELECT id, title, main_gameplay_type, story_focused_flag FROM general_game WHERE title=?;"
        
        var title: String = ""
        var mainGameplayType: String = ""
        var storyFocusedFlag: Int32 = -1
        var id: Int32 = -1
        
        var queryStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, selectStatementStr, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(queryStatement, 1, (title as NSString).utf8String, -1, nil)

            while sqlite3_step(queryStatement) == SQLITE_ROW  {
                id = sqlite3_column_int(queryStatement, 0)
                title = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                mainGameplayType = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                storyFocusedFlag = sqlite3_column_int(queryStatement, 3)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        sqlite3_finalize(queryStatement)
        return GeneralGameModel(id: Int(id), titleStr: title, gameplayType: mainGameplayType, storyFocused: DatabaseHelper.intToBool(val: Int(storyFocusedFlag)))
    }
        
        
}
