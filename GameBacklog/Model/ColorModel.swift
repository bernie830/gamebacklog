//
//  ColorModel.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 12/12/21.
//

import Foundation
import SwiftUI

extension UIColor {

    //static let flatDarkBackground = UIColor(red: 36, green: 36, blue: 36)
    //static let flatDarkCardBackground = UIColor(red: 46, green: 46, blue: 46)

//    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
//        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: a)
//    }
}

extension Color {
    public init(decimalRed red: Double, green: Double, blue: Double) {
        self.init(red: red / 255, green: green / 255, blue: blue / 255)
    }
}

struct ColorMap {
    static let flatDarkCardBackground = Color(decimalRed: 46, green: 46, blue: 46)
    static let redBackground = Color(decimalRed: 240, green: 128, blue: 128)
    static let lightPinkBackground = Color(decimalRed: 206, green: 147, blue: 210)
    static let purpleBackground = Color(decimalRed: 153, green: 147, blue: 210)
    static let lightBlueBackground = Color(decimalRed: 110, green: 180, blue: 237)
    static let lightGreenBackground = Color(decimalRed: 50, green: 180, blue: 120)
    static let lightGreyBackground = Color(decimalRed: 180, green: 180, blue: 180)
    static let platinumColorBackground = Color(decimalRed: 228, green: 227, blue: 226)


    static let white = Color.white
    static let offWhite = Color(decimalRed: 245, green: 245, blue: 245)
    static let grey = Color(decimalRed: 210, green: 210, blue: 210)
    static let darkGrey = Color(decimalRed: 95, green: 95, blue: 95)
    static let black = Color.black
}
