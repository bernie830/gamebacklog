//
//  GamePlatformModel.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/3/22.
//

import Foundation
import SwiftUI


struct GamePlatformModel: Identifiable {
    var id: Int
    var platform: String
    var storefront: String
    var platformReleaseDate: Date
    var releaseWindow: String
    var mostRecentPrice: Double
    var generalGameId: Int

    
    init(id: Int, platformStr: String, storefrontStr: String, releaseWindowStr: String, releaseDate: Date, mostRecentPriceDbl: Double, generalGameId: Int) {
        self.id = id
        self.platform = platformStr
        self.storefront = storefrontStr
        self.releaseWindow = releaseWindowStr
        self.platformReleaseDate = releaseDate
        self.mostRecentPrice = mostRecentPriceDbl
        self.generalGameId = generalGameId
    }
    
    var stringValue : String {
        var retVal = self.platform
        if self.platform.contains("PC") && self.storefront != ""{
            retVal += " (" + self.storefront + ")"
        }
        
        return retVal
    }
    
    var isReleased : Bool {
        var retVal = false
        if self.releaseWindow.count == 0 {
            retVal = self.platformReleaseDate <  Date()
        }
        return retVal

    }
}
