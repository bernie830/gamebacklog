//
//  GeneralGameModel.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 12/16/21.
//

import Foundation
import SwiftUI


struct GeneralGameModel: Identifiable {
    var id: Int
    var title: String
    var mainGameplayType: String
    var isStoryFocused: Bool
    private var gamePlatforms: [GamePlatformModel]
        
    init(id: Int, titleStr: String, gameplayType: String, storyFocused: Bool) {
        self.id = id
        self.title = titleStr
        self.mainGameplayType = gameplayType
        self.isStoryFocused = storyFocused
        self.gamePlatforms = []
    }
}
