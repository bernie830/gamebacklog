//
//  AppModel.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 12/12/21.
//

import Foundation

struct AppModel {
    private static var backlog: DatabaseHelper = DatabaseHelper()

    public static var starterQueueStatuses = ["N/A", "Fell Off", "Unstarted", "Paused", "Play Next", "In Progress", "Completed", "Platinumed"]
    public static var gameplayTypeOptions = ["Single Player", "Multiplayer"]
    public static var platformOptions = ["PC", "Switch", "PS5", "Xbox Series X", "Xbox Series S", "PS4", "Xbox One", "3DS"]
        
    static func getDatabaseModel() -> DatabaseHelper {
        return backlog
    }
}

