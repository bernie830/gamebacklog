//
//  PersonalGameModel.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 12/16/21.
//

import Foundation
import SwiftUI

struct PersonalGameModel: Identifiable {
    var id: Int
    var generalGame: GeneralGameModel
    var queueStatus: QueueSpotModel
    var priceIdPay: Double
    var hasBeenPurchased: Bool
    var gameOnPlatform: GamePlatformModel
        
    init(id: Int, generalGame: GeneralGameModel, platform: GamePlatformModel, queueSpot: QueueSpotModel, priceIdPayDbl: Double, isPurchased: Bool) {
        self.id = id
        self.generalGame = generalGame
        self.gameOnPlatform = platform
        self.queueStatus = queueSpot
        self.priceIdPay = priceIdPayDbl
        self.hasBeenPurchased = isPurchased
    }
    
    var isStoryFocused : Bool {
        self.generalGame.isStoryFocused
    }
    
    var mostRecentPrice : Double {
        self.gameOnPlatform.mostRecentPrice
    }
    
    var title : String {
        self.generalGame.title
    }
    
    var generalGameId : Int32 {
        Int32(self.generalGame.id)
    }
    
    var queueSpotId : Int32 {
        Int32(self.queueStatus.id)
    }
    
    var gamePlatformId : Int32 {
        Int32(self.gameOnPlatform.id)
    }
    
    var isReleased : Bool {
        self.gameOnPlatform.isReleased
    }
    
    public func updateQueueSpot(queueSpot: QueueSpotModel) {
        PersonalGameDBHelper.updateQueueSpot(id: self.id, newQueueSpotId: queueSpot.id)
    }
    
    public func delete() {
        PersonalGameDBHelper.delete(id: self.id)
    }
}
