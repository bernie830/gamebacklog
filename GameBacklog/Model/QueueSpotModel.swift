//
//  QueueSpotModel.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 12/12/21.
//

import Foundation
import SwiftUI
class QueueSpotModel: Identifiable, Equatable, Comparable, Hashable {    
    var queueStringID: String
    var id: Int
    var queueSortId: Int
    var gameRowBackgroundColor: Color
    var gameRowTextColor: Color
    
    init(id: Int, queueString: String, queueSortId: Int, gameRowBackgroundColor: Color, gameRowTextColor: Color) {
        self.queueStringID = queueString
        self.id = id
        self.queueSortId = queueSortId
        self.gameRowBackgroundColor = gameRowBackgroundColor
        self.gameRowTextColor = gameRowTextColor
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    static func < (lhs: QueueSpotModel, rhs: QueueSpotModel) -> Bool {
        lhs.queueSortId > rhs.queueSortId
    }
    
    static func == (left: QueueSpotModel, right: QueueSpotModel) -> Bool {
        return left.queueStringID == right.queueStringID && left.queueSortId == right.queueSortId
    }
    
}
