//
//  TestEnvironment.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/3/22.
//

import Foundation
struct TestEnvironment {
    public static func getTestGame(generalGameId: Int, titleStr: String, gameplayType: String,storyFocused: Bool, platformStr: String, storefrontStr: String, releaseWindowStr: String, queueSpot: QueueSpotModel, isPurchased: Bool, releaseDate: Date, price: Double, priceIdPayDbl: Double) -> PersonalGameModel {
        
        let generalGame = GeneralGameModel(id: generalGameId, titleStr: titleStr, gameplayType: gameplayType, storyFocused: storyFocused)
        let gamePlatform = GamePlatformModel(id: generalGameId + 300, platformStr: platformStr, storefrontStr: storefrontStr, releaseWindowStr: releaseWindowStr, releaseDate: releaseDate, mostRecentPriceDbl: price, generalGameId: generalGameId)
        
        
        
        // Set id to +100 to make testing easier
        let testGame = PersonalGameModel(id: generalGameId + 100, generalGame: generalGame, platform: gamePlatform, queueSpot: queueSpot, priceIdPayDbl: priceIdPayDbl, isPurchased: isPurchased)
        
        return testGame
    }
    

    public static func createStarterQueueSpots() -> [QueueSpotModel]{
        let starterQueueSpots: [QueueSpotModel] = [
            QueueSpotModel(id: 200, queueString: "N/A", queueSortId: 200, gameRowBackgroundColor: ColorMap.flatDarkCardBackground, gameRowTextColor: ColorMap.white),
            QueueSpotModel(id: 201, queueString: "Fell Off", queueSortId: 201, gameRowBackgroundColor: ColorMap.redBackground, gameRowTextColor: ColorMap.white),
            QueueSpotModel(id: 202, queueString: "Unstarted", queueSortId: 202, gameRowBackgroundColor: ColorMap.lightPinkBackground, gameRowTextColor: ColorMap.white),
            QueueSpotModel(id: 203, queueString: "Paused", queueSortId: 203, gameRowBackgroundColor: ColorMap.purpleBackground, gameRowTextColor: ColorMap.white),
            QueueSpotModel(id: 204, queueString: "Play Next", queueSortId: 204, gameRowBackgroundColor: ColorMap.lightBlueBackground, gameRowTextColor: ColorMap.white),
            QueueSpotModel(id: 205, queueString: "In Progress", queueSortId: 205, gameRowBackgroundColor: ColorMap.lightGreenBackground, gameRowTextColor: ColorMap.white),
            QueueSpotModel(id: 206, queueString: "Completed", queueSortId: 206, gameRowBackgroundColor: ColorMap.lightGreyBackground, gameRowTextColor: ColorMap.white),
            QueueSpotModel(id: 206, queueString: "Platinumed", queueSortId: 206, gameRowBackgroundColor: ColorMap.platinumColorBackground, gameRowTextColor: ColorMap.white)
        
        
        ]

        return starterQueueSpots
    }
    
    public static func getQueueSpotWithStatus(queueSpots: [QueueSpotModel], queueStatus: String) -> QueueSpotModel {
        var retVal: QueueSpotModel? = nil
        for queueSpot in queueSpots {
            if queueSpot.queueStringID == queueStatus {
                retVal = queueSpot
            }
        }
        return retVal!
    }
    
    public static func getSubsetOfFakeData(queueSpotString: String) -> [PersonalGameModel] {
        let fakeData = TestEnvironment.getFakeData()
        var retVal: [PersonalGameModel] = []
        for game in fakeData {
            if game.queueStatus.queueStringID == queueSpotString {
                retVal.append(game)
            }
        }
        
        return retVal
    }
    
    public static func getFakeData() -> [PersonalGameModel] {
        let queueSpots = createStarterQueueSpots()
        
        let games = [
            
            getTestGame(generalGameId: 10, titleStr: "Pillars of Eternity", gameplayType: "Single Player",storyFocused: true, platformStr: "PC", storefrontStr: "Windows Store", releaseWindowStr: "",  queueSpot: getQueueSpotWithStatus(queueSpots: queueSpots, queueStatus: "Fell Off"), isPurchased: true, releaseDate: DatabaseHelper.dbStringFormatToDate(date: "2015-05-26"), price: 60.0, priceIdPayDbl: 0.0),
            getTestGame(generalGameId: 1, titleStr: "Dyson Sphere Program", gameplayType: "Single Player", storyFocused: false, platformStr: "PC", storefrontStr: "Steam", releaseWindowStr: "", queueSpot: getQueueSpotWithStatus(queueSpots: queueSpots, queueStatus:"Completed"), isPurchased: true, releaseDate: DatabaseHelper.dbStringFormatToDate(date: "2021-01-21"),  price: 20.0, priceIdPayDbl: 0.0),
            getTestGame(generalGameId: 2, titleStr: "VALORANT", gameplayType: "Multiplayer", storyFocused: false, platformStr: "PC", storefrontStr: "Other", releaseWindowStr: "", queueSpot: getQueueSpotWithStatus(queueSpots: queueSpots, queueStatus:"In Progress"), isPurchased: true, releaseDate: DatabaseHelper.dbStringFormatToDate(date: "2020-06-02"), price: 0.0, priceIdPayDbl: 0.0),
            getTestGame(generalGameId: 3, titleStr: "Final Fantasy XIV", gameplayType: "Multiplayer", storyFocused: true, platformStr: "PC", storefrontStr: "Steam", releaseWindowStr: "",  queueSpot: getQueueSpotWithStatus(queueSpots: queueSpots, queueStatus:"Play Next"), isPurchased: true, releaseDate: DatabaseHelper.dbStringFormatToDate(date: "2013-08-27"), price: 0.0, priceIdPayDbl: 0.0),
            getTestGame(generalGameId: 4, titleStr: "Mass Effect Legendary Edition", gameplayType: "Single Player", storyFocused: true, platformStr: "PC", storefrontStr: "Steam", releaseWindowStr: "",  queueSpot: getQueueSpotWithStatus(queueSpots: queueSpots, queueStatus:"Play Next"), isPurchased: false, releaseDate: DatabaseHelper.dbStringFormatToDate(date: "2021-05-14"), price: 60.0, priceIdPayDbl: 45.0),
            getTestGame(generalGameId: 5, titleStr: "Cyberpunk 2077", gameplayType: "Single Player", storyFocused: true, platformStr: "PC", storefrontStr: "GOG", releaseWindowStr: "", queueSpot: getQueueSpotWithStatus(queueSpots: queueSpots, queueStatus:"Paused"), isPurchased: true, releaseDate: DatabaseHelper.dbStringFormatToDate(date: "2020-12-10"), price: 60.0, priceIdPayDbl: 0.0),
            getTestGame(generalGameId: 6, titleStr: "Death Stranding", gameplayType: "Single Player",storyFocused: true, platformStr: "PC", storefrontStr: "Steam", releaseWindowStr: "", queueSpot: getQueueSpotWithStatus(queueSpots: queueSpots, queueStatus:"Unstarted"), isPurchased: false, releaseDate: DatabaseHelper.dbStringFormatToDate(date: "2020-06-02"), price: 60.0, priceIdPayDbl: 15.0),
            getTestGame(generalGameId: 7, titleStr: "Persona 4 Golden", gameplayType: "Single Player",storyFocused: true, platformStr: "PC", storefrontStr: "Steam", releaseWindowStr: "",  queueSpot: getQueueSpotWithStatus(queueSpots: queueSpots, queueStatus:"Unstarted"), isPurchased: false, releaseDate: DatabaseHelper.dbStringFormatToDate(date: "2020-06-13"), price: 20.0, priceIdPayDbl: 7.0),
            getTestGame(generalGameId: 8, titleStr: "Spider-Man Miles Morales", gameplayType: "Single Player",storyFocused: true, platformStr: "PS5", storefrontStr: "", releaseWindowStr: "Summer 2021",  queueSpot: getQueueSpotWithStatus(queueSpots: queueSpots, queueStatus:"Unstarted"), isPurchased: true, releaseDate: DatabaseHelper.dbStringFormatToDate(date: "2023-11-12"), price: 60.0, priceIdPayDbl: 30.0),
            getTestGame(generalGameId: 9, titleStr: "Yakuza: Like a Dragon", gameplayType: "Single Player", storyFocused: true, platformStr: "PC", storefrontStr: "Steam", releaseWindowStr: "", queueSpot: getQueueSpotWithStatus(queueSpots: queueSpots, queueStatus:"Fell Off"), isPurchased: true, releaseDate: DatabaseHelper.dbStringFormatToDate(date: "2020-01-16"), price: 60.0, priceIdPayDbl: 0.0)
        ]

        return games
    }
}
