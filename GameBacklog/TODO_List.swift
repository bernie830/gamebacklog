//
//  TODO_List.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/5/22.
//

import Foundation

/*
 - Delete all delete files
 - Push to git
 - Figure out how to push the queue move button to be pinned in the top right
 - Connect it to be able to add games
    - Handle the flow where a GeneralGame has not already been created
    - Handle the case where a GeneralGame has been created
 - Add settings to be able to choose own QueueStatuses customize colors
    - We'll keep the starter ones the same probably
    - Include a reset to original button or something that sets it back and changes all of the ones that were custom to like "Unstarted" or something
 - Think about filters and if we want them at all?
 - Remove references to tags and _old from ListRowView and PersonalGameModel
 - Fully hide the lines
 - Figure out best way to remove games entirely (swipe would be great but is only available in iOS 15 - Maybe just add a pop up to the current method)
 - Get the back button to work with correctly filled out info in the add flow
 
 
 
 
 
 
 
 */
