//
//  BacklogViewModel.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/13/22.
//

import Foundation


class StatusRowViewModel : ObservableObject, Identifiable, Hashable {
    private var queueSpot: QueueSpotModel
    @Published private var gamesInStatus : [GamePreviewViewModel] = []
    @Published private var listViewModelParent: ListViewModel?
    
    
    /*
     Constructors
     */
    init(queuSpotModel: QueueSpotModel, listViewModel: ListViewModel, gameModels: [PersonalGameModel]){
        self.queueSpot = queuSpotModel
        self.listViewModelParent = listViewModel
        
        for game in gameModels {
            let gamePreviewViewModel = GamePreviewViewModel(game: game, parentViewModel: self)
            self.gamesInStatus.append(gamePreviewViewModel)
        }
    }
    
    
    /*
     Getters
     */
    var allQueueStatuses : [QueueSpotModel] {
        self.listViewModelParent!.allQueueStatuses
    }
    
    var queueSpotDescriptor : String {
        queueSpot.queueStringID
    }
    
    var gamePreviews : [GamePreviewViewModel] {
        gamesInStatus
    }
    
    
    /*
     Public Functions
     */
    func moveGameQueueSpots(gamePreview: GamePreviewViewModel, toQueueSpotId: String) {
        // remove it from the current status view
        self.remove(gamePreview: gamePreview)
        
        // add it to the next status view
        for statusRow in listViewModelParent!.statusRows {
            if statusRow.queueSpotDescriptor == toQueueSpotId {
                statusRow.add(viewModel: gamePreview)
            }
        }
    }
    
    func add(viewModel: GamePreviewViewModel) {
        self.gamesInStatus.append(viewModel)
    }
    
    func remove(gamePreview: GamePreviewViewModel) {
        var index = 0
        while index < gamesInStatus.count && gamesInStatus[index].id != gamePreview.id {
            index = index + 1
        }
        self.gamesInStatus.remove(at: index)
    }
    
    func addGameInStatus(gameModel: PersonalGameModel) {
        let gamePreviewViewModel = GamePreviewViewModel(game: gameModel, parentViewModel: self)
        self.gamesInStatus.append(gamePreviewViewModel)
    }
    
    
    /*
     Conform to Protocol Functions
     */
    func hash(into hasher: inout Hasher) {
        hasher.combine(queueSpot.id)
    }
    
    static func == (lhs: StatusRowViewModel, rhs: StatusRowViewModel) -> Bool {
        lhs.queueSpot.id == rhs.queueSpot.id
    }
}
