//
//  ListViewModel.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/15/22.
//

import Foundation

class ListViewModel : ObservableObject, Identifiable {
    @Published private var filteredBacklogViewModels : [StatusRowViewModel] = []
    @Published private var allBacklogViewModels : [StatusRowViewModel] = []
    @Published private var refreshCtr : Int = 0
    private var queueStatuses: [QueueSpotModel] = []
    
    
    /*
     Getters
     */
    var allCount : Int{
        self.allBacklogViewModels.count
    }
    
    var statusRows : [StatusRowViewModel] {
        if self.allBacklogViewModels.count == 0 {
            fetchQueueStatuses()
            fetchGames()
        }
        return self.allBacklogViewModels
    }
    
    var allQueueStatuses : [QueueSpotModel] {
        self.queueStatuses
    }
    
    
    /*
     Public Functions
     */
    func moveGameQueueSpots(gamePreview: GamePreviewViewModel, fromQueueSpotId: String, toQueueSpotId: String) {
        var index = 0
        print(allBacklogViewModels.count)

        while index < self.allBacklogViewModels.count {
            let currViewModel = self.allBacklogViewModels[index]
            if currViewModel.queueSpotDescriptor == fromQueueSpotId {
                //currViewModel.remove(index: index)
            } else if currViewModel.queueSpotDescriptor == toQueueSpotId {
                currViewModel.add(viewModel: gamePreview)
            } else {
                index = index + 1
            }
        }
    }
    
    
    /*
     Private Functions
     */
    private func getQueueSpotIndex(viewModel: [StatusRowViewModel], queueSpotString: String) -> Int{
        var index = 0
        while index < viewModel.count && viewModel[index].queueSpotDescriptor != queueSpotString {
            index = index + 1
        }
        return index
    }
    
    private func fetchQueueStatuses() {
        queueStatuses = AppModel.getDatabaseModel().getAllQueueSpots()
        queueStatuses.sort()
        
        allBacklogViewModels = []
        filteredBacklogViewModels = []
        for queueSpot in queueStatuses {
            let statusRowViewModel = StatusRowViewModel(queuSpotModel: queueSpot, listViewModel: self, gameModels: [])
            allBacklogViewModels.append(statusRowViewModel)
            filteredBacklogViewModels.append(statusRowViewModel)
        }
    }
    
    private func fetchGames() {
        let allGames = AppModel.getDatabaseModel().getAllGames()

        for game in allGames {
            let statusRowViewModel = allBacklogViewModels[getQueueSpotIndex(viewModel: allBacklogViewModels, queueSpotString: game.queueStatus.queueStringID)]
            
            statusRowViewModel.addGameInStatus(gameModel: game)
        }
    }
}
