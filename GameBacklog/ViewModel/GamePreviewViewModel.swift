//
//  GamePreviewViewModel.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/14/22.
//

import Foundation
import SwiftUI

class GamePreviewViewModel : ObservableObject, Identifiable {
    @Published private var game : PersonalGameModel
    @Published private var queueSpot: QueueSpotModel
    @Published private var statusRowParent: StatusRowViewModel
    
    
    /*
     Constructors
     */
    init(game: PersonalGameModel, parentViewModel: StatusRowViewModel) {
        self.game = game
        self.queueSpot = game.queueStatus
        self.statusRowParent = parentViewModel
    }
    
    
    /*
     Getters
     */
    var activeQueueSpots: [QueueSpotModel] {
        self.statusRowParent.allQueueStatuses
    }
    
    var backgroundColor : Color {
        queueSpot.gameRowBackgroundColor
    }
    
    var textColor : Color {
        game.isReleased ? ColorMap.white : ColorMap.grey
    }
    
    var title : String {
        game.title + " - " + game.gameOnPlatform.stringValue
    }
    
    var subtitle : String {
        var subtitle = ""
        
        subtitle = addTagToSubtitle(subtitle: subtitle, stringToAdd: getReleaseDateString())
            
        if self.game.isStoryFocused {
            subtitle = addTagToSubtitle(subtitle: subtitle, stringToAdd: "Story Focused")
        } else {
            subtitle = addTagToSubtitle(subtitle: subtitle, stringToAdd: "Gameplay Focused")
        }
            
        if self.game.mostRecentPrice == 0 {
            subtitle = addTagToSubtitle(subtitle: subtitle, stringToAdd: "Free")
        } else if self.game.hasBeenPurchased {
            subtitle = addTagToSubtitle(subtitle: subtitle, stringToAdd: "Purchased")
        }
        return subtitle
    }
    
    
    /*
     Public functions
     */
    func setQueueSpot(queueSpot: QueueSpotModel) {
        let newStringId = queueSpot.queueStringID
        
        game.queueStatus = queueSpot
        PersonalGameDBHelper.updateQueueSpot(id: game.id, newQueueSpotId: queueSpot.id)


        self.queueSpot = queueSpot
        self.statusRowParent.moveGameQueueSpots(gamePreview: self, toQueueSpotId: newStringId)
    }
    
    func deleteGame() {
        self.statusRowParent.remove(gamePreview: self)
        game.delete()
    }
    
    
    /*
     Private functions
     */
    private func getReleaseDateString() -> String {
        var releaseDateSubtitle = ""
        
        if self.game.gameOnPlatform.releaseWindow.count == 0 {
            if self.game.gameOnPlatform.platformReleaseDate > Date() {
                let formatter = DateFormatter()
                formatter.setLocalizedDateFormatFromTemplate("dd-MM-yyyy")
                releaseDateSubtitle = "Releases " + formatter.string(from: self.game.gameOnPlatform.platformReleaseDate)
            }
        } else {
            releaseDateSubtitle = "Releases " + self.game.gameOnPlatform.releaseWindow
        }
        
        return releaseDateSubtitle
    }
    
    private func addTagToSubtitle(subtitle: String, stringToAdd: String) -> String {
        var retVal = subtitle
        if retVal.count > 0 {
            retVal = retVal + " | "
        }
        return retVal + stringToAdd
    }
}
