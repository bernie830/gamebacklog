//
//  ListView.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 12/12/21.
//

import Foundation
import SwiftUI

struct ListView: View {
    @ObservedObject var listViewModel = ListViewModel()
        
    var body: some View {
        ZStack {
            if listViewModel.statusRows.count > 0 {
                ScrollView {
                    ForEach(listViewModel.statusRows, id: \.self) { statusRowView in
                        StatusRowView(statusRowViewModel: statusRowView)
                    }
                }
            }
        }.background(ColorMap.black)
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
