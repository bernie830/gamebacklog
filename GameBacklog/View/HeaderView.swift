//
//  HeaderView.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/16/22.
//

import Foundation
import SwiftUI

struct HeaderView: View {
    @Binding var addNewGame: Bool
    
    var body: some View {
        HStack() {
            Text("Games Backlog").fontWeight(.heavy).padding(8.0).font(.title).lineLimit(1)
            Spacer()
            Button(action: {
                print("Edit Button action")
            }) {
                Image(systemName: "gearshape.fill").font(.system(size: 25))
            }.frame(height: nil)
            Button(action: {
                print("Button action")
                addNewGame = true
            }) {
                Image(systemName: "plus").font(.system(size: 25))
            }.padding(.trailing, 20.0).frame(height: nil)
        }
    }
}

struct HeaderView_Previews: PreviewProvider {
    @State static var addNewGame: Bool = false
    static var previews: some View {
        HeaderView(addNewGame: $addNewGame).background(ColorMap.black).foregroundColor(.gray)
    }
}
