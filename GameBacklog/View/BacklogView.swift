//
//  ListPageView.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 6/12/21.
//

import Foundation
import SwiftUI

struct BacklogView: View {
    @State var addNewGame: Bool = false
    
    var body: some View {
        ZStack {
            Color.black.ignoresSafeArea()
            VStack () {
                HeaderView(addNewGame: $addNewGame)
                if !addNewGame {
                    ListView().background(ColorMap.black)
                } else {
                    Text("Adding Game?")
                    //CreateGameMainView(addNewGame: $addNewGame)
                }
            }
            .foregroundColor(.gray)
        }
    }
}


struct BacklogView_Previews: PreviewProvider {
    static var previews: some View {
        BacklogView()
    }
}
