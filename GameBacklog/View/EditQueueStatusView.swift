//
//  EditQueueStatusView.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 2/25/22.
//

import Foundation
import SwiftUI

struct EditQueueStatusView: View {
    @ObservedObject var editQueueStatusViewModel: EditQueueStatusViewModel
        
    var body: some View {
        ZStack {
            ColorMap.black.ignoresSafeArea()
            VStack() {
                Spacer()
                ColorView(color: editQueueStatusViewModel.statusBackgroundColor).frame(width: 125, height: 125, alignment: .center)
                TextField("Title", text: $editQueueStatusViewModel.statusTitle).textFieldStyle(RoundedBorderTextFieldStyle()).multilineTextAlignment(.center).foregroundColor(ColorMap.black).padding(.horizontal, 50).padding(.top, 50)
                BinarySelectorView(textField: "Primary Queue Spot?", options: ["Yes", "No"], selectedValue: $editQueueStatusViewModel.isPrimaryString, primaryColor: editQueueStatusViewModel.statusBackgroundColor, secondaryColor: ColorMap.white, textColor: ColorMap.grey).padding(.horizontal, 40)

                Spacer()
            }
        }.background(ColorMap.black)
    }
}

struct EditQueueStatusView_Previews: PreviewProvider {
    static var previews: some View {
        let editQueueStatusViewModel = EditQueueStatusViewModel(status: TestEnvironment.getQueueSpotWithStatus(queueSpots: TestEnvironment.createStarterQueueSpots(), queueStatus: "Unstarted"))
        EditQueueStatusView(editQueueStatusViewModel: editQueueStatusViewModel)
        
    }
}
