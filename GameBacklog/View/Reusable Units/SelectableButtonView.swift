//
//  SelectableButtonView.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 7/25/21.
//

import Foundation
import SwiftUI


struct SelectableButtonView: View {
    @Binding var boundButtonValue: String
    var buttonValue: String
    
    var body: some View {
        ZStack {
            Rectangle().strokeBorder(boundButtonValue != buttonValue ? ColorMap.darkGrey : ColorMap.black, lineWidth: 1).background(Rectangle().fill(boundButtonValue != buttonValue ? ColorMap.darkGrey : ColorMap.offWhite)).onTapGesture {
                boundButtonValue = buttonValue
            }
            Text(buttonValue).foregroundColor(boundButtonValue == buttonValue ? ColorMap.darkGrey : ColorMap.offWhite)
        }
    }

}

struct SelectableButtonView_Previews: PreviewProvider {
    @State static var boundButtonValue = "PC"
    static var previews: some View {
        SelectableButtonView(boundButtonValue: $boundButtonValue, buttonValue: "PC")
    }
}
