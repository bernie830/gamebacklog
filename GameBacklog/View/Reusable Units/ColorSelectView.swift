//
//  ColorSelectView.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 2/25/22.
//

import Foundation
import SwiftUI
struct ColorSelectView: View {
    var rowCount: Int = 8
    
    var body: some View {
        VStack {
            ForEach(0..< rowCount) {i in
                HStack {
                    ForEach(0..<viewModel.columnCount) {j in
                        if((i*viewModel.columnCount) + j < viewModel.allPlatformsOptions.count) {
                            MultiSelectableButtonView(boundButtonValues: $selected, buttonValue: viewModel.allPlatformsOptions[(i*viewModel.columnCount) + j], primaryColor: viewModel.primaryColor, secondaryColor: viewModel.secondaryColor, multiSelect: viewModel.multiSelect, selectable: viewModel.selectable).frame(width: viewModel.width, height: viewModel.height, alignment: .center)
                        }
                    }
                }
            }
        }
    }
}

struct ColorSelectView_Previews: PreviewProvider {
    
    static var previews: some View {
        ZStack() {
            ColorMap.black.ignoresSafeArea()
            ColorSelectView()
        }
    }
}
