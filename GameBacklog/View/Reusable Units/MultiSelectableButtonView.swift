//
//  SelectableButtonView.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 7/25/21.
//

import Foundation
import SwiftUI


struct MultiSelectableButtonView: View {
    @Binding var boundButtonValues: [String]
    var buttonValue: String
    
    var body: some View {
        ZStack {
            Rectangle().strokeBorder(!boundButtonValues.contains(buttonValue) ? ColorMap.darkGrey : ColorMap.black, lineWidth: 1).background(Rectangle().fill(!boundButtonValues.contains(buttonValue) ? ColorMap.darkGrey : ColorMap.offWhite)).onTapGesture {
                if boundButtonValues.contains(buttonValue) {
                    boundButtonValues.remove(at: boundButtonValues.firstIndex(of: buttonValue)!)
                } else {
                    boundButtonValues.append(buttonValue)
                }
            }
            Text(buttonValue).foregroundColor(boundButtonValues.contains(buttonValue) ? ColorMap.darkGrey : ColorMap.offWhite)
        }
    }

}

struct MultiSelectableButtonView_Previews: PreviewProvider {
    @State static var boundButtonValues = ["PC"]
    static var previews: some View {
        MultiSelectableButtonView(boundButtonValues: $boundButtonValues, buttonValue: "PC")
    }
}
