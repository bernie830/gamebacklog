//
//  ListRowView.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 12/12/21.
//

import Foundation
import SwiftUI

struct GamePreviewView: View {
    
    @ObservedObject private var gamePreviewViewModel: GamePreviewViewModel
    
    init(gamePreviewViewModel : GamePreviewViewModel){
        self.gamePreviewViewModel = gamePreviewViewModel
    }
    
    var body: some View {
        ZStack(alignment: .leading) {
            VStack(alignment: .leading) {
                HStack {
                    Text(gamePreviewViewModel.title)
                        .font(.title3)
                        .fontWeight(.heavy)
                        .lineLimit(2)
                        .foregroundColor(gamePreviewViewModel.textColor)
                    Spacer()
                    Menu {
                        ForEach(gamePreviewViewModel.activeQueueSpots, id:\.self) { newQueueSpot in
                            Button {
                                gamePreviewViewModel.setQueueSpot(queueSpot: newQueueSpot)
                            } label: {
                                Text(newQueueSpot.queueStringID)
                            }
                        }
                        Button {
                            gamePreviewViewModel.deleteGame()
                        } label: {
                            Text("Remove")
                        }
                        
                    } label: {
                        Image(systemName: "ellipsis").frame(width: 25.0, height: 18.0)
                            .font(.system(size: 25))
                    }
                }.padding(.horizontal, 17).padding(.top, 10)
                Text(gamePreviewViewModel.subtitle).lineLimit(1)
                    .padding(.vertical, 10)
                    .font(.footnote).scaledToFit().minimumScaleFactor(0.01).padding(.horizontal, 17)
                
            }
            .foregroundColor(ColorMap.white).background(gamePreviewViewModel.backgroundColor)
            
            
        }
        .clipShape(RoundedRectangle(cornerRadius: 15))
    }
}

struct GamePreviewViewTest_Previews: PreviewProvider {

    static var previews: some View {
        let queueSpot1 = TestEnvironment.getQueueSpotWithStatus(queueSpots: TestEnvironment.createStarterQueueSpots(), queueStatus: "In Progress")

        let queueSpot2 = TestEnvironment.getQueueSpotWithStatus(queueSpots: TestEnvironment.createStarterQueueSpots(), queueStatus: "Unstarted")
        
        let game = TestEnvironment.getTestGame(generalGameId: 1, titleStr: "Valorant", gameplayType: "Multiplayer", storyFocused: false, platformStr: "PC", storefrontStr: "Other", releaseWindowStr: "", queueSpot: queueSpot1, isPurchased: true, releaseDate: DatabaseHelper.dbStringFormatToDate(date: "2021-11-24"), price: 0.0, priceIdPayDbl: 0.0)
        let game2 = TestEnvironment.getTestGame(generalGameId: 1, titleStr: "Spider-Man Miles Morales", gameplayType: "Single Player", storyFocused: false, platformStr: "PS5", storefrontStr: "", releaseWindowStr: "Summer 2021", queueSpot: queueSpot2, isPurchased: true, releaseDate: DatabaseHelper.dbStringFormatToDate(date: "2021-11-12"), price: 60.0, priceIdPayDbl: 60.0)
        
        

        let listViewModel1 = ListViewModel()
        let listViewModel2 = ListViewModel()

        let statusRowViewModel1 = StatusRowViewModel(queuSpotModel: queueSpot1, listViewModel: listViewModel1, gameModels: [])
        let statusRowViewModel2 = StatusRowViewModel(queuSpotModel: queueSpot2, listViewModel: listViewModel2, gameModels: [])


        GamePreviewView(gamePreviewViewModel: GamePreviewViewModel(game: game, parentViewModel: statusRowViewModel1)).padding(.horizontal, 20.0)
        GamePreviewView(gamePreviewViewModel: GamePreviewViewModel(game: game2, parentViewModel: statusRowViewModel2)).padding(.horizontal, 20.0)
    }
}
