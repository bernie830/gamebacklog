//
//  StatusRowView.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 1/14/22.
//

import Foundation
import SwiftUI

struct StatusRowView: View {
    @ObservedObject private var statusRowViewModel: StatusRowViewModel
    
    init(statusRowViewModel : StatusRowViewModel){
        self.statusRowViewModel = statusRowViewModel
    }
    
    var body: some View {
        ZStack {
            ColorMap.black
            VStack {
                HStack {
                    Text(statusRowViewModel.queueSpotDescriptor).fontWeight(.heavy).font(.title2).foregroundColor(ColorMap.offWhite)
                    Spacer()
                }.padding(.vertical, 3)
                
                ForEach(statusRowViewModel.gamePreviews, id: \.id) { gamePreviewViewModel in
                    GamePreviewView(gamePreviewViewModel: gamePreviewViewModel).padding(.horizontal, 10.0).padding(.bottom, 3)
                }
            }.padding(.horizontal, 10.0)
        }
    }
}

struct StatusRowView_Previews: PreviewProvider {
    static var previews: some View {
        let status = "Unstarted"
        let queueSpotModel = QueueSpotModel(id: 202, queueString: status, queueSortId: 202, gameRowBackgroundColor: ColorMap.lightPinkBackground, gameRowTextColor: ColorMap.white)
        
        let fakeGamesData = TestEnvironment.getSubsetOfFakeData(queueSpotString: status)
        let listViewModel = ListViewModel()
        let abc = listViewModel.statusRows


        let statusRowViewModel = StatusRowViewModel(queuSpotModel: queueSpotModel, listViewModel: listViewModel, gameModels: fakeGamesData)

        StatusRowView(statusRowViewModel: statusRowViewModel)
        
    }
}
