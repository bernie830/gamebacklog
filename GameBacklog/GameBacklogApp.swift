//
//  GameBacklogApp.swift
//  GameBacklog
//
//  Created by Hunter Bernhardt on 6/8/21.
//

import SwiftUI



@main
struct GameBacklogApp: App {

    var body: some Scene {
        WindowGroup {
            BacklogView()
        }
    }
}


